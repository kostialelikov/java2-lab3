package ua.kpi.fpm.pzks.FS;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Binary file")
public class BinaryFileTest {
    @Test
    @DisplayName("Create file")
    void createFileTest() {
        assertThrows(IllegalArgumentException.class,
                () -> BinaryFile.create("test", null, "sample".getBytes()));

        var dir = Directory.create("some", null);
        assertThrows(IllegalArgumentException.class, () -> BinaryFile.create("", dir, "test".getBytes()));
        assertThrows(IllegalArgumentException.class, () -> BinaryFile.create("name", dir, null));

        var file = BinaryFile.create("f", dir, "some data".getBytes());
        assertNotNull(file);
        assertEquals(Entity.EntityType.BINARY_FILE, file.getType());
        assertEquals("f", file.getName());
        assertFalse(file.isDirectory());
    }

    @Test
    @DisplayName("Read bytes")
    void readBytesTest() {
        var dir = Directory.create("d", null);
        var file = BinaryFile.create("f", dir, "some data".getBytes());
        assertArrayEquals("some data".getBytes(), file.read());

        var emptyFile = BinaryFile.create("k", dir, "".getBytes());
        assertEquals(0, emptyFile.read().length);
    }
}
