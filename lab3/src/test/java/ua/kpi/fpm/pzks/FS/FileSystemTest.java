package ua.kpi.fpm.pzks.FS;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("File system")
public class FileSystemTest {
    @Test
    @DisplayName("Move files")
    void moveFilesTest() {
        var root = Directory.create("", null);
        var namedPipe = NamedPipe.create("pipe", root);
        var homeDir = Directory.create("home", root);
        var userDir = Directory.create("user", homeDir);

        namedPipe.move("/home/user/");
        assertFalse(userDir.list().isEmpty());
        assertEquals(namedPipe, userDir.list().get(0));

        userDir.move("/");
        assertTrue(root.list().contains(userDir));
    }

    @Test
    @DisplayName("Delete files")
    void deleteFilesTest() {
        var root = Directory.create("", null);
        var homeDir = Directory.create("home", root);
        var namedPipe = NamedPipe.create("pipe", homeDir);
        var tempDir = Directory.create("user", homeDir);

        namedPipe.delete();
        assertFalse(homeDir.list().contains(namedPipe));
        assertTrue(homeDir.list().contains(tempDir));
        tempDir.delete();
        assertFalse(homeDir.list().contains(tempDir));
        homeDir.delete();
        assertTrue(root.list().isEmpty());
    }
}
