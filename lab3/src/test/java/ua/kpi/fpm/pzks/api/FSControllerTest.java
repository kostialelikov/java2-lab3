package ua.kpi.fpm.pzks.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Base64;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@DisplayName("FS Controller")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class FSControllerTest {
    @Autowired
    private MockMvc mvc;

    private static ObjectNode usrDirObjCreate;
    private static ObjectNode logTextFileObjCreate;
    private static ObjectNode logTextFileObjUpdate;

    @BeforeAll
    static void setup() {
        var objectMapper = new ObjectMapper();
        usrDirObjCreate = objectMapper.createObjectNode();
        usrDirObjCreate.put("entity_type", "DIRECTORY");
        usrDirObjCreate.put("entity_name", "usr");
        usrDirObjCreate.put("directory_path", "/");

        logTextFileObjCreate = objectMapper.createObjectNode();
        logTextFileObjCreate.put("entity_type", "LOG_TEXT_FILE");
        logTextFileObjCreate.put("entity_name", "temp.log");
        logTextFileObjCreate.put("directory_path", "/usr");
        logTextFileObjCreate.put("data", "HERE log N1 ...");

        logTextFileObjUpdate = objectMapper.createObjectNode();
        logTextFileObjUpdate.put("data", " TesT  !!!!");
    }

    @Test
    @Order(1)
    @DisplayName("Create")
    public void createTest() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/api/v1/fs/entities/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(usrDirObjCreate.toPrettyString()))
                    .andExpect(status().isOk())
                    .andExpect(content().json("{\"name\":\"usr\",\"" +
                            "directory\":true,\"type\":\"DIRECTORY\",\"size\":0,\"fullPath\":\"/usr\"}"));

        mvc.perform(MockMvcRequestBuilders.post("/api/v1/fs/entities/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(logTextFileObjCreate.toPrettyString()))
                    .andExpect(status().isOk())
                    .andExpect(content().json("{\"name\":\"temp.log\",\"type\":" +
                            "\"LOG_TEXT_FILE\",\"size\":15,\"directory\":false,\"fullPath\":\"/usr/temp.log\"}"));

        mvc.perform(MockMvcRequestBuilders.post("/api/v1/fs/entities/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(logTextFileObjCreate.toPrettyString()))
                    .andExpect(status().isBadRequest());
    }

    @Test
    @Order(2)
    @DisplayName("Tree")
    public void treeTest() throws Exception {
        var encodedPath = new String(Base64.getEncoder().encode("/usr".getBytes()));
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/fs/entities/" + encodedPath))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"name\":\"usr\",\"type\":\"DIRECTORY\",\"size\":0," +
                        "\"children\":[{\"name\":\"temp.log\",\"type\":\"LOG_TEXT_FILE\",\"size\":15}]}"));

        var wrongPathEncoded = new String(Base64.getEncoder().encode("/home".getBytes()));
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/fs/entities/" + wrongPathEncoded))
                .andExpect(status().isNotFound());

    }

    @Test
    @Order(3)
    @DisplayName("Append and read")
    public void appendAndReadTest() throws Exception {
        var encodedPath = new String(Base64.getEncoder().encode("/usr/temp.log".getBytes()));
        mvc.perform(MockMvcRequestBuilders.put("/api/v1/fs/entities/" + encodedPath)
                .contentType(MediaType.APPLICATION_JSON)
                .content(logTextFileObjUpdate.toPrettyString()))
                    .andExpect(status().isOk())
                    .andExpect(content().json("{\"name\":\"temp.log\",\"type\":\"LOG_TEXT_FILE\"," +
                            "\"size\":26,\"data\":\"HERE log N1 ... TesT  !!!!\"}"));

        mvc.perform(MockMvcRequestBuilders.get("/api/v1/fs/entities/" + encodedPath + "/content"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"data\":\"HERE log N1 ... TesT  !!!!\"}"));

        var wrongPathEncoded = new String(Base64.getEncoder().encode("/usr".getBytes()));
        mvc.perform(MockMvcRequestBuilders.get("/api/v1/fs/entities/" + wrongPathEncoded + "/content"))
                .andExpect(status().isBadRequest());

    }


    @Test
    @Order(4)
    @DisplayName("Delete")
    public void deleteTest() throws Exception {
        var encodedPathFile = new String(Base64.getEncoder().encode("/usr/temp.log".getBytes()));
        var encodedPathDir = new String(Base64.getEncoder().encode("/usr".getBytes()));

        mvc.perform(MockMvcRequestBuilders.delete("/api/v1/fs/entities/" + encodedPathDir))
                .andExpect(status().isBadRequest());

        mvc.perform(MockMvcRequestBuilders.delete("/api/v1/fs/entities/" + encodedPathFile))
                .andExpect(status().isNoContent())
                .andExpect(content().string(""));

        mvc.perform(MockMvcRequestBuilders.delete("/api/v1/fs/entities/" + encodedPathDir))
                .andExpect(status().isNoContent())
                .andExpect(content().string(""));
    }
}
