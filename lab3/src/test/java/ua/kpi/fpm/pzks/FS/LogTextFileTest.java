package ua.kpi.fpm.pzks.FS;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Log text file")
public class LogTextFileTest {
    @Test
    @DisplayName("Create file")
    void createFileTest() {
        assertThrows(IllegalArgumentException.class,
                () -> LogTextFile.create("test", null, "data"));

        var dir = Directory.create("some", null);
        assertThrows(IllegalArgumentException.class, () -> LogTextFile.create("name", dir, null));

        var file = LogTextFile.create("file sample", dir, "line");
        assertNotNull(file);
        assertEquals(Entity.EntityType.LOG_TEXT_FILE, file.getType());
        assertEquals("file sample", file.getName());
        assertFalse(file.isDirectory());
    }

    @Test
    @DisplayName("Read append")
    void readAppendTest() {
        var dir = Directory.create("dir", null);
        var file = LogTextFile.create("file", dir, "data some");

        assertEquals("data some", file.read());
        file.append("\nanother data");
        assertEquals("data some\nanother data", file.read());
        file.append("");
        assertEquals("data some\nanother data", file.read());
    }
}
