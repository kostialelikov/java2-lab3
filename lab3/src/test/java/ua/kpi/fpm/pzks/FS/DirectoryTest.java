package ua.kpi.fpm.pzks.FS;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Directory")
public class DirectoryTest {
    @Test
    @DisplayName("Create directory")
    void createDirectoryTest() {
        assertThrows(IllegalArgumentException.class,
                () -> Directory.create("", Directory.create("test", null)));

        var dir = Directory.create("some", null);
        assertNotNull(dir);

        assertEquals(Entity.EntityType.DIRECTORY, dir.getType());
        assertEquals("root", dir.getName());
        assertTrue(dir.isDirectory());

        var subDir = Directory.create("tmp", dir);
        assertEquals("tmp", subDir.getName());
    }

    @Test
    @DisplayName("list directory")
    void listDirectoryTest() {
        var root = Directory.create("", null);
        var listElements = new ArrayList<Entity>();
        listElements.add(Directory.create("sub", root));
        listElements.add(BinaryFile.create("file.bin", root, "data".getBytes()));
        listElements.add(LogTextFile.create("log", root, "line"));
        assertEquals(listElements, root.list());
        assertEquals(Collections.emptyList(), Directory.create("", null).list());
    }

    @Test
    @DisplayName("delete directory")
    void deleteDirectoryTest() {
        var root = Directory.create("", null);
        var file = BinaryFile.create("file", root, "data".getBytes());
        var subDir = Directory.create("var", root);

        assertThrows(IllegalCallerException.class, root::delete);
        assertDoesNotThrow(subDir::delete);
        assertDoesNotThrow(file::delete);
        assertDoesNotThrow(root::delete);
    }

    @Test
    @DisplayName("move directory")
    void moveSubdirectoriesAndFilesTest() {
        var root = Directory.create("", null);
        var subDir = Directory.create("folder", root);
        var anotherSubDir = Directory.create("dir", root);
        var log = LogTextFile.create("l.txt", subDir, "\n");

        assertThrows(IllegalCallerException.class, () -> root.move(subDir));
        assertDoesNotThrow(() -> anotherSubDir.move("/" + subDir.getName()));
        assertTrue(subDir.list().contains(anotherSubDir));
        assertTrue(subDir.list().contains(log));
    }

    @Test
    @DisplayName("Add many entities to directory")
    void addManyEntitiesToDirTest() {
        var root = Directory.create("", null);

        for (int i = 0; i < 25; i++) {
            BufferFile.create("name" + i, root);
        }
        assertEquals(25, root.list().size());
        assertThrows(IllegalCallerException.class, () -> BufferFile.create("test", root));
    }
}
