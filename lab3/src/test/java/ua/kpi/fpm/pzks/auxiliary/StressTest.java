package ua.kpi.fpm.pzks.auxiliary;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Stress test")
public class StressTest {
    @Test
    @DisplayName("perform stress test")
    void stressTest() {
        var controller = new Controller();
        var generator = new Generator(controller);
        assertDoesNotThrow(() -> generator.performFSStressTest(3000));
    }
}
