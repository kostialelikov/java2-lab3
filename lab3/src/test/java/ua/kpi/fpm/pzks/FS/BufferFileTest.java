package ua.kpi.fpm.pzks.FS;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Buffer file")
public class BufferFileTest {
    @Test
    @DisplayName("Create file")
    void createFileTest() {
        assertThrows(IllegalArgumentException.class, () -> BufferFile.create("test", null));

        var dir = Directory.create("some", null);
        assertThrows(IllegalArgumentException.class, () -> BufferFile.create("name/name", dir));
        assertThrows(IllegalArgumentException.class, () -> BufferFile.create("", dir));

        var file = BufferFile.create("file sample", dir);
        assertNotNull(file);
        assertEquals(Entity.EntityType.BUFFER_FILE, file.getType());
        assertEquals("file sample", file.getName());
        assertFalse(file.isDirectory());
    }

    @Test
    @DisplayName("Consume and push")
    void consumePushTest() {
        var dir = Directory.create("dir", null);
        var file = BufferFile.<Character>create("file", dir);

        assertThrows(NoSuchElementException.class, file::consume);
        file.push('Q');
        file.push('A');
        assertEquals(Character.valueOf('Q'), file.consume());
        assertEquals(Character.valueOf('A'), file.consume());
        assertThrows(NoSuchElementException.class, file::consume);

        assertThrows(IllegalArgumentException.class, () -> file.push(null));
    }

    @Test
    @DisplayName("Push many elements")
    void pushManyElementsTest() {
        var dir = Directory.create("dir", null);
        var file = BufferFile.<Character>create("file", dir);

        for (int i = 0; i < 10; i++) {
            file.push((char) ('A' + i));
        }
        assertThrows(IllegalCallerException.class, () -> file.push('Z'));
    }
}