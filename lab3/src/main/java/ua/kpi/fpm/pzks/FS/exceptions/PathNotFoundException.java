package ua.kpi.fpm.pzks.FS.exceptions;

public class PathNotFoundException extends RuntimeException {
    public PathNotFoundException() {
        super("Provided path not found");
    }
}
