package ua.kpi.fpm.pzks.FS.exceptions;

public class InvalidTypeOfFileException extends RuntimeException {
    public InvalidTypeOfFileException() {
        super("Invalid type of file");
    }
}

