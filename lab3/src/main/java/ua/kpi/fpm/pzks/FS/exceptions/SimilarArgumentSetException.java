package ua.kpi.fpm.pzks.FS.exceptions;

public class SimilarArgumentSetException extends Exception {
    public SimilarArgumentSetException() {
        super("Similar argument has been set already");
    }
}
