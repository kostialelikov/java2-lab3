package ua.kpi.fpm.pzks.auxiliary;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.kpi.fpm.pzks.FS.*;
import ua.kpi.fpm.pzks.FS.actions.*;
import ua.kpi.fpm.pzks.FS.actions.List;
import ua.kpi.fpm.pzks.FS.exceptions.ArgumentOverrideException;
import ua.kpi.fpm.pzks.FS.exceptions.SimilarArgumentSetException;

import java.util.*;
import java.util.concurrent.Future;

public class Generator {
    private final static Logger logger = LoggerFactory.getLogger(Generator.class);
    private final Controller controller;
    private final java.util.List<Directory> dirs;
    private final java.util.List<BinaryFile> binaryFiles;
    private final java.util.List<BufferFile<Character>> bufferFiles;
    private final java.util.List<LogTextFile> logTextFiles;
    private final java.util.List<Future<?>> futures;

    public Generator(Controller controller) {
        this.controller = controller;
        this.dirs = new ArrayList<>();
        dirs.add(controller.getRoot());
        dirs.add(Directory.create("home", controller.getRoot()));
        binaryFiles = new ArrayList<>();
        binaryFiles.add(BinaryFile.create("f.bin", controller.getRoot(), new byte[] {0, 1}));
        bufferFiles = new ArrayList<>();
        bufferFiles.add(BufferFile.create("hm.buf", controller.getRoot()));
        logTextFiles = new ArrayList<>();
        logTextFiles.add(LogTextFile.create("test.log", controller.getRoot(), "test\n"));
        futures = new LinkedList<>();
    }

    public void performFSStressTest() throws ArgumentOverrideException, SimilarArgumentSetException {
        performFSStressTest(1000);
    }

    public void performFSStressTest(int millis) throws ArgumentOverrideException, SimilarArgumentSetException {
        performFSStressTest(millis, FSAction.FSActionType.values());
    }

    public void performFSStressTest(int millis, FSAction.FSActionType[] actionTypes)
            throws ArgumentOverrideException, SimilarArgumentSetException {
        long startTime = System.currentTimeMillis();
        var rnd = new Random();

        while (System.currentTimeMillis() - startTime < millis) {
            FSAction.FSActionType cur = actionTypes[getRandomIntInRegion(rnd, 0, actionTypes.length - 1)];
            switch (cur) {
                case APPEND: {
                    var log = getRandomFromList(rnd, logTextFiles);
                    var action = new Append(log);
                    action.setArgument(UUID.randomUUID().toString());
                    futures.add(controller.submitFSAction(action));
                } continue;
                case CONSUME: {
                    var buff = getRandomFromList(rnd, bufferFiles);
                    var action = new Consume<>(buff);
                    futures.add(controller.submitFSAction(action));
                } continue;
                case COUNT: {
                    var dir = getRandomFromList(rnd, dirs);
                    var action = new Count(dir);
                    action.setArgument(true);
                    futures.add(controller.submitFSAction(action));
                } continue;
                case CREATE: {
                    if (rnd.nextBoolean()) {
                        var action = new Create<>(BufferFile.class);
                        action.setNameArgument("file" + UUID.randomUUID().toString());
                        action.setDirectoryArgument(getRandomFromList(rnd, dirs));
                        futures.add(controller.submitFSAction(action));
                    } else {
                        var action = new Create<>(Directory.class);
                        action.setNameArgument("dir" + UUID.randomUUID().toString());
                        action.setDirectoryArgument(getRandomFromList(rnd, dirs));
                        futures.add(controller.submitFSAction(action));
                    }
                } continue;
                case CREATE_BINARY_FILE: {
                    var action = new CreateBinaryFile();
                    action.setBytesArgument(new byte[] {0, 1, 3});
                    action.setNameArgument("bin" + UUID.randomUUID().toString());
                    action.setDirectoryArgument(getRandomFromList(rnd, dirs));
                    futures.add(controller.submitFSAction(action));
                } continue;
                case CREATE_LOG_TEXT_FILE: {
                    var action = new CreateLogTextFile();
                    action.setDataArgument(UUID.randomUUID().toString());
                    action.setNameArgument("log" + UUID.randomUUID().toString());
                    action.setDirectoryArgument(getRandomFromList(rnd, dirs));
                    futures.add(controller.submitFSAction(action));
                } continue;
                case GET_ENTITIES_WITH_SIZE_IN_RANGE:  {
                    var dir = getRandomFromList(rnd, dirs);
                    var action = new GetEntitiesWithSizeInRange(dir);
                    action.setLowerBoundArgument(0);
                    action.setUpperBoundArgument(rnd.nextInt(300));
                    futures.add(controller.submitFSAction(action));
                } continue;
                case GET_NAME: {
                    var entity = getRandomEntity(rnd);
                    var action = new GetName(entity);
                    futures.add(controller.submitFSAction(action));
                } continue;
                case GET_SIZE: {
                    var entity = getRandomEntity(rnd);
                    var action = new GetSize(entity);
                    futures.add(controller.submitFSAction(action));
                } continue;
                case GET_TYPE: {
                    var entity = getRandomEntity(rnd);
                    var action = new GetType(entity);
                    futures.add(controller.submitFSAction(action));
                } continue;
                case IS_DIRECTORY: {
                    var entity = getRandomEntity(rnd);
                    var action = new IsDirectory(entity);
                    futures.add(controller.submitFSAction(action));
                } continue;
                case LIST: {
                    var dir = getRandomFromList(rnd, dirs);
                    var action = new List(dir);
                    futures.add(controller.submitFSAction(action));
                } continue;
                case PUSH: {
                    var buff = getRandomFromList(rnd, bufferFiles);
                    var action = new Push<>(buff);
                    action.setArgument(getRandomChar(rnd));
                    futures.add(controller.submitFSAction(action));
                } continue;
                case READ: {
                    if (rnd.nextBoolean()) {
                        var log = getRandomFromList(rnd, logTextFiles);
                        var action = new Read<>(log);
                        futures.add(controller.submitFSAction(action));
                    } else {
                        var bin = getRandomFromList(rnd, binaryFiles);
                        var action = new Read<>(bin);
                        futures.add(controller.submitFSAction(action));
                    }
                } continue;
                case SEARCH: {
                    var dir = getRandomFromList(rnd, dirs);
                    var action = new Search(dir);
                    action.setArgument("file");
                    futures.add(controller.submitFSAction(action));
                } continue;
                case TREE: {
                    var dir = getRandomFromList(rnd, dirs);
                    var action = new Tree(dir);
                    futures.add(controller.submitFSAction(action));
                }
            }
        }
        logger.debug(controller.getRoot().tree());
        controller.stopWorkers();
    }

    private int getRandomIntInRegion(Random rand, int min, int max) {
        return min + rand.nextInt(max - min + 1);
    }

    private <U> U getRandomFromList(Random rand, java.util.List<U> list) {
        return list.get(getRandomIntInRegion(rand, 0, list.size() - 1));
    }

    private char getRandomChar(Random rand) {
        return (char)(getRandomIntInRegion(rand, 0,26) + 'a');
    }

    private Entity getRandomEntity(Random rand) {
        int totalLength = dirs.size() + binaryFiles.size() + bufferFiles.size() + logTextFiles.size();
        int target = getRandomIntInRegion(rand, 0, totalLength - 1);
        if (target < dirs.size()) {
            return dirs.get(target);
        }
        target -= dirs.size();
        if (target < binaryFiles.size()) {
            return binaryFiles.get(target);
        }
        target -= binaryFiles.size();
        if (target < bufferFiles.size()) {
            return bufferFiles.get(target);
        }
        target -= bufferFiles.size();
        return logTextFiles.get(target);
    }
}
