package ua.kpi.fpm.pzks.FS.actions;

import ua.kpi.fpm.pzks.FS.Entity;

public class GetType extends FSAction<Void, Entity> {
    public GetType(Entity target) {
        super(target);
    }

    public GetType(String targetPath) {
        super(targetPath);
    }

    @Override
    public FSActionType getFSActionType() {
        return FSActionType.GET_TYPE;
    }
}
