package ua.kpi.fpm.pzks.FS.actions;

import ua.kpi.fpm.pzks.FS.BufferFile;

public class Push<T> extends FSAction<T, BufferFile<T>> {
    public Push(BufferFile<T> target) {
        super(target);
    }

    public Push(String targetPath) {
        super(targetPath);
    }

    @Override
    public FSActionType getFSActionType() {
        return FSActionType.PUSH;
    }
}
