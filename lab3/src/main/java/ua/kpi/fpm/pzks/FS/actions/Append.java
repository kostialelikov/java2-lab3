package ua.kpi.fpm.pzks.FS.actions;

import ua.kpi.fpm.pzks.FS.LogTextFile;

public class Append extends FSAction<String, LogTextFile> {
    public Append(LogTextFile target) {
        super(target);
    }

    public Append(String targetPath) {
        super(targetPath);
    }

    @Override
    public FSActionType getFSActionType() {
        return FSActionType.APPEND;
    }
}
