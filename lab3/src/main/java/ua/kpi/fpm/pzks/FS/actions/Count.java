package ua.kpi.fpm.pzks.FS.actions;

import ua.kpi.fpm.pzks.FS.Directory;

public class Count extends FSAction<Boolean, Directory> {
    public Count(Directory target) {
        super(target);
    }

    public Count(String targetPath) {
        super(targetPath);
    }

    @Override
    public FSActionType getFSActionType() {
        return FSActionType.COUNT;
    }
}
