package ua.kpi.fpm.pzks.FS.actions;

import ua.kpi.fpm.pzks.FS.BinaryFile;
import ua.kpi.fpm.pzks.FS.exceptions.ArgumentNotSetException;
import ua.kpi.fpm.pzks.FS.exceptions.ArgumentOverrideException;

import java.util.Arrays;

public class CreateBinaryFile extends Create<BinaryFile> {
    private byte[] data;

    public CreateBinaryFile() {
        super(BinaryFile.class);
    }

    public void setBytesArgument(byte[] data) throws ArgumentOverrideException {
        if (this.data != null) {
            throw new ArgumentOverrideException();
        }
        this.data = Arrays.copyOf(data, data.length);
    }

    public byte[] getBytesArgument() throws ArgumentNotSetException {
        return Arrays.copyOf(data, data.length);
    }

    @Override
    public FSActionType getFSActionType() {
        return FSActionType.CREATE_BINARY_FILE;
    }
}
