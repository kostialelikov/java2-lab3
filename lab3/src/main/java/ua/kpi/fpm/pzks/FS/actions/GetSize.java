package ua.kpi.fpm.pzks.FS.actions;

import ua.kpi.fpm.pzks.FS.Entity;

public class GetSize extends FSAction<Void, Entity> {
    public GetSize(Entity target) {
        super(target);
    }

    public GetSize(String targetPath) {
        super(targetPath);
    }

    @Override
    public FSActionType getFSActionType() {
        return FSActionType.GET_SIZE;
    }
}
