package ua.kpi.fpm.pzks.FS.actions;

import ua.kpi.fpm.pzks.FS.Entity;
import ua.kpi.fpm.pzks.FS.exceptions.ArgumentNotSetException;
import ua.kpi.fpm.pzks.FS.exceptions.ArgumentOverrideException;

import java.util.Optional;

public abstract class FSAction<T, U extends Entity> {
    public enum FSActionType {
        APPEND,
        CONSUME,
        COUNT,
        CREATE,
        CREATE_BINARY_FILE,
        CREATE_LOG_TEXT_FILE,
        DELETE,
        GET_ENTITIES_WITH_SIZE_IN_RANGE,
        GET_NAME,
        GET_SIZE,
        GET_TYPE,
        IS_DIRECTORY,
        LIST,
        MOVE,
        PUSH,
        READ,
        SEARCH,
        TREE,
        WRITE
    }

    private T argument = null;
    private U target;
    private String targetPath;
    protected Class<U> entityType;

    protected FSAction(Class<U> newEntityType) {
        entityType = newEntityType;
    }

    public FSAction(U target) {
        this.target = target;
    }

    public FSAction(String targetPath) {
        this.targetPath = targetPath;
    }

    public Optional<String> getTargetPath() {
        return Optional.ofNullable(targetPath);
    }

    public Optional<U> getTarget() {
        return Optional.ofNullable(target);
    }

    public void setArgument(T arg) throws ArgumentOverrideException {
        if (argument != null) {
            throw new ArgumentOverrideException();
        }
        argument = arg;
    }

    public T getArgument() throws ArgumentNotSetException {
        if (argument == null) {
            throw new ArgumentNotSetException();
        }
        return argument;
    }

    public abstract FSActionType getFSActionType();
}
