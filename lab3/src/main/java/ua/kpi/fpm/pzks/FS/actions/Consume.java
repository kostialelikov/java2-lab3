package ua.kpi.fpm.pzks.FS.actions;

import ua.kpi.fpm.pzks.FS.BufferFile;

public class Consume<T> extends FSAction<Void, BufferFile<T>> {
    public Consume(BufferFile<T> target) {
        super(target);
    }

    public Consume(String targetPath) {
        super(targetPath);
    }

    @Override
    public FSActionType getFSActionType() {
        return FSActionType.CONSUME;
    }
}
