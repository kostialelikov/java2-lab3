package ua.kpi.fpm.pzks.FS.actions;

import ua.kpi.fpm.pzks.FS.*;
import ua.kpi.fpm.pzks.FS.exceptions.ArgumentNotSetException;
import ua.kpi.fpm.pzks.FS.exceptions.ArgumentOverrideException;
import ua.kpi.fpm.pzks.FS.exceptions.InvalidTypeOfFileException;
import ua.kpi.fpm.pzks.FS.exceptions.SimilarArgumentSetException;

import java.util.Optional;

public class Create<T extends Entity> extends FSAction<String, T> {
    private Directory directory = null;
    private String directoryPath = null;

    public Create(Class<T> clazz) {
        super(clazz);
    }

    public static Create<?> getCreateActionFromType(String newEntityType) {
        Class<?> entityType;
        try {
            switch (Entity.EntityType.valueOf(newEntityType)) {
                case DIRECTORY:     entityType = Directory.class;   break;
                case BUFFER_FILE:   entityType = BufferFile.class;  break;
                case NAMED_PIPE:    entityType = NamedPipe.class;   break;
                case BINARY_FILE:   return new CreateBinaryFile();
                case LOG_TEXT_FILE: return new CreateLogTextFile();
                default:            throw new IllegalArgumentException();
            }
        } catch (IllegalArgumentException e) {
            throw new InvalidTypeOfFileException();
        }
        return new Create(entityType);
    }

    public Class<T> getClassOfTarget() {
        return entityType;
    }

    public void setNameArgument(String name) throws ArgumentOverrideException {
        setArgument(name);
    }

    public String getNameArgument() throws ArgumentNotSetException {
        return getArgument();
    }

    public void setDirectoryArgument(Directory dir) throws ArgumentOverrideException, SimilarArgumentSetException {
        if (directory != null) {
            throw new ArgumentOverrideException();
        }
        if (directoryPath != null) {
            throw new SimilarArgumentSetException();
        }
        directory = dir;
    }

    public Optional<Directory> getDirectoryArgument() throws ArgumentNotSetException {
        if (directory == null && directoryPath == null) {
            throw new ArgumentNotSetException();
        }
        return Optional.ofNullable(directory);
    }

    public void setDirectoryPathArgument(String dirPath) throws ArgumentOverrideException, SimilarArgumentSetException {
        if (directoryPath != null) {
            throw new ArgumentOverrideException();
        }
        if (directory != null) {
            throw new SimilarArgumentSetException();
        }
        directoryPath = dirPath;
    }

    public Optional<String> getDirectoryPathArgument() throws ArgumentNotSetException {
        if (directory == null && directoryPath == null) {
            throw new ArgumentNotSetException();
        }
        return Optional.ofNullable(directoryPath);
    }

    @Override
    public FSActionType getFSActionType() {
        return FSActionType.CREATE;
    }
}
