package ua.kpi.fpm.pzks.FS.actions;

import ua.kpi.fpm.pzks.FS.NamedPipe;

public class Write extends FSAction<String, NamedPipe> {
    public Write(NamedPipe target) {
        super(target);
    }

    public Write(String targetPath) {
        super(targetPath);
    }

    @Override
    public FSActionType getFSActionType() {
        return FSActionType.WRITE;
    }
}
