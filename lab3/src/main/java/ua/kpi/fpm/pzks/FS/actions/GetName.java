package ua.kpi.fpm.pzks.FS.actions;

import ua.kpi.fpm.pzks.FS.Entity;

public class GetName extends FSAction<Void, Entity> {
    public GetName(Entity target) {
        super(target);
    }

    public GetName(String targetPath) {
        super(targetPath);
    }

    @Override
    public FSActionType getFSActionType() {
        return FSActionType.GET_NAME;
    }
}
