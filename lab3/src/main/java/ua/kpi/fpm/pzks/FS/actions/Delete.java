package ua.kpi.fpm.pzks.FS.actions;

import ua.kpi.fpm.pzks.FS.Entity;

public class Delete extends FSAction<Void, Entity> {
    public Delete(Entity target) {
        super(target);
    }

    public Delete(String targetPath) {
        super(targetPath);
    }

    @Override
    public FSActionType getFSActionType() {
        return FSActionType.DELETE;
    }
}
