package ua.kpi.fpm.pzks.FS.actions;

import ua.kpi.fpm.pzks.FS.Directory;

public class Tree extends FSAction<Void, Directory> {
    public Tree(Directory target) {
        super(target);
    }

    public Tree(String targetPath) {
        super(targetPath);
    }

    @Override
    public FSActionType getFSActionType() {
        return FSActionType.TREE;
    }
}
