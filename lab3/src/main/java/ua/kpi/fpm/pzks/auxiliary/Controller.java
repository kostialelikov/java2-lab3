package ua.kpi.fpm.pzks.auxiliary;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.kpi.fpm.pzks.FS.*;
import ua.kpi.fpm.pzks.FS.actions.*;
import ua.kpi.fpm.pzks.FS.markers.Readable;
import ua.kpi.fpm.pzks.FS.exceptions.ArgumentNotSetException;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Controller {
    private final static Logger logger = LoggerFactory.getLogger(Controller.class);
    private final Directory root;
    private ExecutorService workers;
    private int amountOfWorkerThreads;

    public Controller() {
        this(Runtime.getRuntime().availableProcessors());
    }

    public Controller(int amountOfWorkerThreads) {
        logger.info("Started with number of threads: " + amountOfWorkerThreads);
        this.amountOfWorkerThreads = amountOfWorkerThreads;
        root = Directory.create("root", null);
        workers = Executors.newFixedThreadPool(amountOfWorkerThreads);
    }

    public Directory getRoot() {
        return root;
    }

    public void setAmountOfWorkerThreads(int val) {
        amountOfWorkerThreads = val;
        workers = Executors.newFixedThreadPool(amountOfWorkerThreads);
    }

    public void stopWorkers() {
        workers.shutdown();
    }

    public <T> Future<T> submitFSAction(FSAction<?, ?> action) {
        switch (action.getFSActionType()) {
            case APPEND: {
                var target = (LogTextFile) loadTarget(action);
                String data = tryLoadArgument(action);
                return (Future<T>) workers.submit(() -> target.append(data));
            }
            case CONSUME: {
                var target = (BufferFile<T>) loadTarget(action);
                return workers.submit(target::consume);
            }
            case COUNT: {
                var target = (Directory) loadTarget(action);
                boolean isRecursive = tryLoadArgument(action);
                return (Future<T>) workers.submit(() -> target.count(isRecursive));
            }
            case CREATE: {
                var createAction = (Create<?>)action;
                var type = ((Create<?>)action).getClassOfTarget();
                try {
                    var name = createAction.getNameArgument();
                    var dir = loadDir(action);
                    if (type.equals(BufferFile.class)) {
                        return (Future<T>) workers.submit(() -> BufferFile.create(name, dir));
                    } else if (type.equals(Directory.class)) {
                        return (Future<T>) workers.submit(() -> Directory.create(name, dir));
                    } else {
                        return (Future<T>) workers.submit(() -> NamedPipe.create(name, dir));
                    }
                } catch (ArgumentNotSetException e) {
                    logger.error("No arguments provided");
                    throw new IllegalArgumentException("Need arguments");
                }
            }
            case CREATE_BINARY_FILE: {
                var createAction = (CreateBinaryFile)action;
                try {
                    var name = createAction.getNameArgument();
                    var dir = loadDir(action);
                    var data = createAction.getBytesArgument();
                    return (Future<T>) workers.submit(() -> BinaryFile.create(name, dir, data));
                } catch (ArgumentNotSetException e) {
                    logger.error("No arguments provided");
                    throw new IllegalArgumentException("Need arguments");
                }
            }
            case CREATE_LOG_TEXT_FILE: {
                var createAction = (CreateLogTextFile)action;
                try {
                    var name = createAction.getNameArgument();
                    var dir = loadDir(action);
                    var data = createAction.getDataArgument();
                    return (Future<T>) workers.submit(() -> LogTextFile.create(name, dir, data));
                } catch (ArgumentNotSetException e) {
                    logger.error("No arguments provided");
                    throw new IllegalArgumentException("Need arguments");
                }
            }
            case DELETE: {
                var target = loadTarget(action);
                return (Future<T>) workers.submit(() -> target.delete());
            }
            case GET_ENTITIES_WITH_SIZE_IN_RANGE: {
                var target = (Directory) loadTarget(action);
                int lo = tryLoadArgument(action);
                int hi;
                try {
                    var curAction = (GetEntitiesWithSizeInRange) action;
                    hi = curAction.getUpperBoundArgument();
                } catch (ArgumentNotSetException e) {
                    logger.error("No argument provided");
                    throw new IllegalArgumentException("Need argument");
                }
                return (Future<T>) workers.submit(() -> target.getEntitiesWithSizeInRange(lo, hi));
            }
            case GET_NAME: {
                return (Future<T>) workers.submit(() -> loadTarget(action).getName());
            }
            case GET_SIZE: {
                return (Future<T>) workers.submit(() -> loadTarget(action).getSize());
            }
            case GET_TYPE: {
                return (Future<T>) workers.submit(() -> loadTarget(action).getType());
            }
            case IS_DIRECTORY: {
                return (Future<T>) workers.submit(() -> loadTarget(action).isDirectory());
            }
            case LIST: {
                return (Future<T>) workers.submit(() -> ((Directory)loadTarget(action)).list());
            }
            case MOVE: {
                var moveAction = (Move) action;
                var target = loadTarget(action);
                try {
                    if (moveAction.getDestinationArgument().isPresent()) {
                        var destination = moveAction.getDestinationArgument().get();
                        return (Future<T>) workers.submit(() -> target.move(destination));
                    } else {
                        var destinationPath = moveAction.getDestinationPathArgument().get();
                        return (Future<T>) workers.submit(() -> target.move(destinationPath));
                    }
                } catch (ArgumentNotSetException e) {
                    logger.error("Couldn't get arg. Action is not correct");
                    throw new IllegalArgumentException("No destination specified");
                }
            }
            case PUSH: {
                var target = (BufferFile<T>) loadTarget(action);
                T arg = tryLoadArgument(action);
                return (Future<T>) workers.submit(() -> target.push(arg));
            }
            case READ: {
                var target = (Readable<T>) loadTarget(action);
                return workers.submit(target::read);
            }
            case SEARCH:{
                var target = (Directory) loadTarget(action);
                String pattern = tryLoadArgument(action);
                return (Future<T>) workers.submit(() -> target.search(pattern));
            }
            case TREE: {
                return (Future<T>) workers.submit(() -> ((Directory)loadTarget(action)).treeJSON());
            }
            case WRITE: {
                var target = (NamedPipe) loadTarget(action);
                String data = tryLoadArgument(action);
                return (Future<T>) workers.submit(() -> target.write(data));
            }
            default: throw new IllegalArgumentException("No such action type");
        }
    }

    private Entity loadTarget(FSAction<?, ?> action) {
        Entity res;
        if (action.getTarget().isPresent()) {
            res = action.getTarget().get();
        } else if (action.getTargetPath().isPresent()) {
            res = root.getEntityByPath(action.getTargetPath().get());
        } else {
            logger.error("Couldn't get target. Action is not correct");
            throw new IllegalArgumentException("No target specified");
        }
        return res;
    }

    private Directory loadDir(FSAction<?, ?> action) {
        var createAction = (Create<?>) action;
        try {
            if (createAction.getDirectoryArgument().isPresent()) {
                return createAction.getDirectoryArgument().get();
            } else {
                return (Directory) root.getEntityByPath(createAction.getDirectoryPathArgument().get());
            }
        } catch (ArgumentNotSetException e) {
            logger.error("No argument provided");
            throw new IllegalArgumentException("Need argument");
        }
    }

    private <U> U tryLoadArgument(FSAction<?, ?> action) {
        U data;
        try {
            data = (U) action.getArgument();
        } catch (ArgumentNotSetException e) {
            logger.error("No argument provided");
            throw new IllegalArgumentException("Need argument");
        }
        return data;
    }
}
