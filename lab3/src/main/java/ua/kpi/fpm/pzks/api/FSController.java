package ua.kpi.fpm.pzks.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import ua.kpi.fpm.pzks.FS.Entity;
import ua.kpi.fpm.pzks.FS.LogTextFile;
import ua.kpi.fpm.pzks.FS.actions.*;
import ua.kpi.fpm.pzks.FS.exceptions.ArgumentOverrideException;
import ua.kpi.fpm.pzks.FS.exceptions.InvalidTypeOfFileException;
import ua.kpi.fpm.pzks.FS.exceptions.PathNotFoundException;
import ua.kpi.fpm.pzks.FS.exceptions.SimilarArgumentSetException;
import ua.kpi.fpm.pzks.auxiliary.Controller;

import java.util.Base64;
import java.util.concurrent.ExecutionException;


@RestController
@RequestMapping(value = "api/v1/fs", consumes = "application/json", produces = "application/json")
public class FSController {
    private static final Controller controller = new Controller();

    @PostMapping(value = "/entities")
    public Entity create(@RequestBody JsonNode payload) {
        Create<?> createAction;

        try {
            createAction = Create.getCreateActionFromType(getStringFromRequest("entity_type", payload));
        } catch (InvalidTypeOfFileException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }

        try {
            createAction.setNameArgument(getStringFromRequest("entity_name", payload));
            createAction.setDirectoryPathArgument(getStringFromRequest("directory_path", payload));
            if (createAction instanceof CreateLogTextFile) {
                ((CreateLogTextFile)createAction).setDataArgument(getStringFromRequest("data", payload));
            } else if (createAction instanceof CreateBinaryFile) {
                ((CreateBinaryFile)createAction).setBytesArgument(
                        getStringFromRequest("bytes", payload).getBytes());
            }
        } catch (ArgumentOverrideException | SimilarArgumentSetException e) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }

        try {
            return (Entity) controller.submitFSAction(createAction).get();
        } catch (PathNotFoundException | IllegalArgumentException | InterruptedException | ExecutionException e) {
            if (isInstanceWithCause(e, IllegalArgumentException.class)
                    || isInstanceWithCause(e, PathNotFoundException.class)) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
            }
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }
    }

    @GetMapping(value = "/entities/{encodedPath}", consumes = MediaType.ALL_VALUE)
    public JsonNode tree(@PathVariable String encodedPath) {
        Tree treeAction;
        String path;

        try {
            path = new String(Base64.getDecoder().decode(encodedPath));
            treeAction = new Tree(path);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }

        try {
            JsonNode res;
            try {
                res = (JsonNode) controller.submitFSAction(treeAction).get();
            } catch (ClassCastException | ExecutionException e) {
                if (isInstanceWithCause(e, ClassCastException.class)) {
                    res = createObjectNodeForEntity(path);
                } else {
                    throw e;
                }
            }
            return res;
        } catch (PathNotFoundException | InterruptedException | ExecutionException e) {
            handleError(e);
            return null;
        }
    }

    @PutMapping(value = "/entities/{encodedPath}")
    public JsonNode append(@PathVariable String encodedPath, @RequestBody JsonNode payload) {
        Append appendAction;
        String path;

        try {
            path = new String(Base64.getDecoder().decode(encodedPath));
            appendAction = new Append(path);
            appendAction.setArgument(getStringFromRequest("data", payload));
        } catch (ArgumentOverrideException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }

        try {
            controller.submitFSAction(appendAction).get();
            var data = controller.submitFSAction(new Read<LogTextFile>(path)).get();
            var obj = createObjectNodeForEntity(path);
            obj.set("data", new ObjectMapper().convertValue(data, JsonNode.class));
            return obj;
        } catch (Exception e) {
            handleError(e);
            return null;
        }
    }

    @GetMapping(value = "/entities/{encodedPath}/content", consumes = MediaType.ALL_VALUE)
    public JsonNode read(@PathVariable String encodedPath) {
        Read<?> readAction;
        String path;

        try {
            path = new String(Base64.getDecoder().decode(encodedPath));
            readAction = new Read<>(path);;
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }

        try {
            var data = controller.submitFSAction(readAction).get();
            var objectMapper = new ObjectMapper();
            var obj = objectMapper.createObjectNode();
            obj.set("data", objectMapper.convertValue(data, JsonNode.class));
            return obj;
        } catch (Exception e) {
            handleError(e);
            return null;
        }
    }

    @DeleteMapping(value = "/entities/{encodedPath}", consumes = MediaType.ALL_VALUE)
    public ResponseEntity<Void> delete(@PathVariable String encodedPath) {
        Delete deleteAction;
        String path;

        try {
            path = new String(Base64.getDecoder().decode(encodedPath));
            deleteAction = new Delete(path);;
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
        }

        try {
            controller.submitFSAction(deleteAction).get();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            handleError(e);
            return null;
        }
    }

    private void handleError(Throwable e) {
        if (isInstanceWithCause(e, PathNotFoundException.class)) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage(), e);
        }
        if (e instanceof InterruptedException) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
    }

    private ObjectNode createObjectNodeForEntity(String path) throws ExecutionException, InterruptedException {
        var name = (String) controller.submitFSAction(new GetName(path)).get();
        var type = (Entity.EntityType) controller.submitFSAction(new GetType(path)).get();
        var size = (Integer) controller.submitFSAction(new GetSize(path)).get();
        var mapper = new ObjectMapper();
        var obj = mapper.createObjectNode();
        obj.set("name", mapper.convertValue(name, JsonNode.class));
        obj.set("type", mapper.convertValue(type, JsonNode.class));
        obj.set("size", mapper.convertValue(size, JsonNode.class));
        return obj;
    }

    private static boolean isInstanceWithCause(Throwable e, Class<?> clazz) {
        return clazz.isInstance(e) || (e instanceof ExecutionException && clazz.isInstance(e.getCause()));
    }

    private static String getStringFromRequest(String keyName, JsonNode obj) {
        var value = getJsonNodeFromRequest(keyName, obj);
        if (!value.isTextual()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Property `" + keyName + "` in request JSON object must be a string");
        }
        return value.asText();
    }

    private static JsonNode getJsonNodeFromRequest(String keyName, JsonNode obj) {
        if (!obj.isObject()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Request body must be a JSON object");
        }
        if (!obj.hasNonNull(keyName)) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "Request JSON object must have `" + keyName + "` property");
        }
        return obj.get(keyName);
    }
}
