package ua.kpi.fpm.pzks.FS;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import ua.kpi.fpm.pzks.FS.helpers.HelperRecursiveAction;
import ua.kpi.fpm.pzks.FS.helpers.TreeTraversalAction;

import java.util.*;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Semaphore;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class Directory extends Entity {
    private static final int DIR_MAX_ELEMS = 25;
    private Map<String, Entity> children = new HashMap<>(DIR_MAX_ELEMS);
    private final Semaphore semaphore;
    private int countReaders;

    private Directory() {
        super("root");
        semaphore = new Semaphore(1);
    }

    private Directory(String name, Directory parent) {
        super(name, parent);
        semaphore = new Semaphore(1);
    }

    protected void atomicSynchronizedOperationWithChildren(Runnable fn) {
        semaphore.acquireUninterruptibly();
        try {
            fn.run();
        } finally {
            semaphore.release();
        }
    }

    protected void acquireChildren() {
        semaphore.acquireUninterruptibly();
    }

    protected void releaseChildren() {
        semaphore.release();
    }

    protected <T> T atomicSynchronizedOperationWithTree(Supplier<T> fn) {
        forEachAncestors(Directory::acquireChildren);
        try {
            return fn.get();
        } finally {
            forEachAncestors(Directory::releaseChildren);
        }
    }

    private void forEachAncestors(Consumer<Directory> fn) {
        Directory cur = this;
        while (cur != null) {
            fn.accept(cur);
            cur = cur.getParent();
        }
    }

    protected boolean containsChild(String childName) {
        return children.containsKey(childName);
    }

    protected int countChildren() {
        return children.size();
    }

    protected Entity getChild(String childName) {
        return children.get(childName);
    }

    protected void addChild(Entity child) {
        if (countChildren() >= DIR_MAX_ELEMS) {
            throw new IllegalCallerException("Too many elements in directory");
        }
        children.put(child.getName(), child);
    }

    protected Entity removeChild(String childName) {
        return children.remove(childName);
    }

    protected void updateChild(Entity child, String oldName) {
        children.put(child.getName(), removeChild(oldName));
    }

    protected List<Entity> getChildren() {
        return new ArrayList<>(children.values());
    }

    private void throwExceptionIfDirIsRoot() {
        if (getParent() == null) {
            throw new IllegalCallerException("Root directory couldn't move");
        }
    }

    public static Directory create(String name, Directory parent) {
        if (parent == null) {
            return new Directory();
        }
        return new Directory(name, parent);
    }

    public List<Entity> list() {
        synchronized (semaphore) {
            if (countReaders++ == 0) {
                semaphore.acquireUninterruptibly();
            }
        }
        try {
            return getChildren();
        } finally {
            synchronized (semaphore) {
                if (--countReaders == 0) {
                    semaphore.release();
                }
            }
        }
    }

    public synchronized Entity getEntityByPath(String path) {
        return retrieveEntityFromPath(path);
    }

    @Override
    public void delete() {
        atomicSynchronizedOperationWithChildren(() -> {
            if (countChildren() != 0) {
                throw new IllegalCallerException("Dir not empty");
            }
            super.delete();
        });
    }

    @Override
    public void move(Directory destination) {
        throwExceptionIfDirIsRoot();
        super.move(destination);
    }

    @Override
    public void move(String destination) {
        throwExceptionIfDirIsRoot();
        super.move(destination);
    }

    @Override
    public EntityType getType() {
        return EntityType.DIRECTORY;
    }

    @Override
    public int getSize() {
        return 0;
    }

    @Override
    public boolean isDirectory() {
        return true;
    }

    public int count(boolean recursive) {
        return ForkJoinPool.commonPool().invoke(
                new HelperRecursiveAction<>(this.list(), x -> 1, Integer::sum, () -> 0, recursive));
    }

    public List<Entity> searchEntities(String pattern) {
        return ForkJoinPool.commonPool().invoke(
                getListByMappingRecursively(
                        x -> x.getName().matches(pattern) ? List.of(x) : new LinkedList<>())
                );
    }

    public List<String> search(String pattern) {
        return ForkJoinPool.commonPool().invoke(
                getListByMappingRecursively(
                        x -> x.getName().matches(pattern) ? List.of(x.getFullPath()) : new LinkedList<>())
        );
    }

    public JsonNode treeJSON() {
        ObjectMapper mapper = new ObjectMapper();
        return ForkJoinPool.commonPool().invoke(
                new TreeTraversalAction<>(
                        this,
                        x -> {
                            var obj = mapper.createObjectNode();
                            obj.put("name", x.getName());
                            obj.put("type", x.getType().toString());
                            obj.put("size", x.getSize());
                            return obj;
                        },
                        (dir, entity) -> {
                            if (dir.has("children")) {
                                var arrNode = (ArrayNode) dir.get("children");
                                arrNode.add(entity);
                                dir.set("children", arrNode);
                            } else {
                                var arrNode = mapper.createArrayNode();
                                arrNode.add(entity);
                                dir.set("children", arrNode);
                            }
                            return dir;
                        }
                )
        );
    }

    public String tree() {
        return treeJSON().toPrettyString();
    }

    public List<Entity> getEntitiesWithSizeInRange(int lowerBoundBytes, int upperBoundBytes) {
        return ForkJoinPool.commonPool().invoke(
                getListByMappingRecursively(
                        x -> {
                            var list = new LinkedList<Entity>();
                            if (x.getSize() >= lowerBoundBytes && x.getSize() < upperBoundBytes) {
                                list.add(x);
                            }
                            return list;
                        }));
    }

    private <R> HelperRecursiveAction<List<R>> getListByMappingRecursively(Function<Entity, List<R>> mapper) {
        return new HelperRecursiveAction<>(
                this.list(),
                mapper,
                (list, x) -> {
                    list.addAll(x);
                    return list;
                },
                LinkedList::new,
                true);
    }
}
