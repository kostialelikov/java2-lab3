package ua.kpi.fpm.pzks.FS.actions;

import ua.kpi.fpm.pzks.FS.Directory;

public class List extends FSAction<Void, Directory> {
    public List(Directory target) {
        super(target);
    }

    public List(String targetPath) {
        super(targetPath);
    }

    @Override
    public FSActionType getFSActionType() {
        return FSActionType.LIST;
    }
}
