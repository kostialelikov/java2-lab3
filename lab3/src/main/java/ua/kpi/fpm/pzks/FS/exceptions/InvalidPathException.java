package ua.kpi.fpm.pzks.FS.exceptions;

public class InvalidPathException extends RuntimeException {
    public InvalidPathException() {
        super("Path is not valid");
    }
}
