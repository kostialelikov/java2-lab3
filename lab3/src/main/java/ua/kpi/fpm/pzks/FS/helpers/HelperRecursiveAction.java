package ua.kpi.fpm.pzks.FS.helpers;

import ua.kpi.fpm.pzks.FS.Directory;
import ua.kpi.fpm.pzks.FS.Entity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;

public class HelperRecursiveAction<R> extends RecursiveTask<R> {
    private static final int THRESHOLD = 4;
    private final List<Entity> nodes;
    private final Function<Entity, R> mapper;
    private final BinaryOperator<R> reducer;
    private final Supplier<R> initialValue;
    private final boolean isRecursive;

    public HelperRecursiveAction(List<Entity> nodes, Function<Entity, R> mapper,
                                 BinaryOperator<R> reducer, Supplier<R> initialValue, boolean recursive) {
        this.nodes = nodes;
        this.mapper = mapper;
        this.reducer = reducer;
        this.initialValue = initialValue;
        isRecursive = recursive;
    }

    @Override
    protected R compute() {
        if (nodes.size() > THRESHOLD) {
            var subTasks = createSubTasks(nodes);
            subTasks.forEach(ForkJoinTask::fork);
            return subTasks.stream().map(ForkJoinTask::join).reduce(initialValue.get(), reducer);
        } else {
            return processing(nodes);
        }
    }

    private List<HelperRecursiveAction<R>> createSubTasks(List<Entity> nodes) {
        var subTasks = new ArrayList<HelperRecursiveAction<R>>();
        int lengthToCompute = nodes.size();

        var nodesPartOne = nodes.subList(0, lengthToCompute / 2);
        var nodesPartTwo = nodes.subList(lengthToCompute / 2, lengthToCompute);

        subTasks.add(new HelperRecursiveAction<>(nodesPartOne, mapper, reducer, initialValue, isRecursive));
        subTasks.add(new HelperRecursiveAction<>(nodesPartTwo, mapper, reducer, initialValue, isRecursive));
        return subTasks;
    }

    private R processing(List<Entity> nodes) {
        R result = initialValue.get();
        for (var cur : nodes) {
            if (cur.isDirectory() && isRecursive) {
                Directory curDir = (Directory) cur;
                var intermediateResult =
                        new HelperRecursiveAction<>(curDir.list(), mapper, reducer, initialValue, true);
                result = reducer.apply(result, reducer.apply(mapper.apply(cur), intermediateResult.invoke()));
            } else {
                result = reducer.apply(result, mapper.apply(cur));
            }
        }
        return result;
    }
}
