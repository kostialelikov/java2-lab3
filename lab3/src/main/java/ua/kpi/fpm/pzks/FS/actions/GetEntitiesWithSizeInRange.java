package ua.kpi.fpm.pzks.FS.actions;

import ua.kpi.fpm.pzks.FS.Directory;
import ua.kpi.fpm.pzks.FS.exceptions.ArgumentNotSetException;
import ua.kpi.fpm.pzks.FS.exceptions.ArgumentOverrideException;

public class GetEntitiesWithSizeInRange extends FSAction<Integer, Directory> {
    private Integer upperBound;

    public GetEntitiesWithSizeInRange(Directory target) {
        super(target);
    }

    public GetEntitiesWithSizeInRange(String targetPath) {
        super(targetPath);
    }

    public void setLowerBoundArgument(int lowerBound) throws ArgumentOverrideException {
        setArgument(lowerBound);
    }

    public int getLowerBoundArgument() throws ArgumentNotSetException {
        return getArgument();
    }

    public void setUpperBoundArgument(int upperBound) throws ArgumentOverrideException {
        if (this.upperBound != null) {
            throw new ArgumentOverrideException();
        }
        this.upperBound = upperBound;
    }

    public int getUpperBoundArgument() throws ArgumentNotSetException {
        if (upperBound == null) {
            throw new ArgumentNotSetException();
        }
        return upperBound;
    }

    @Override
    public FSActionType getFSActionType() {
        return FSActionType.GET_ENTITIES_WITH_SIZE_IN_RANGE;
    }
}
