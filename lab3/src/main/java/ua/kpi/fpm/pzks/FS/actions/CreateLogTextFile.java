package ua.kpi.fpm.pzks.FS.actions;

import ua.kpi.fpm.pzks.FS.LogTextFile;
import ua.kpi.fpm.pzks.FS.exceptions.ArgumentNotSetException;
import ua.kpi.fpm.pzks.FS.exceptions.ArgumentOverrideException;

public class CreateLogTextFile extends Create<LogTextFile> {
    private String data;

    public CreateLogTextFile() {
        super(LogTextFile.class);
    }

    public void setDataArgument(String data) throws ArgumentOverrideException {
        if (this.data != null) {
            throw new ArgumentOverrideException();
        }
        this.data = data;
    }

    public String getDataArgument() throws ArgumentNotSetException {
        if (data == null) {
            throw new ArgumentNotSetException();
        }
        return data;
    }

    @Override
    public FSActionType getFSActionType() {
        return FSActionType.CREATE_LOG_TEXT_FILE;
    }
}
