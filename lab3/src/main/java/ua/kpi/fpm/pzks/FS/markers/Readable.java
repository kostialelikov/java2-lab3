package ua.kpi.fpm.pzks.FS.markers;

public interface Readable<T> {
    T read();
}
