package ua.kpi.fpm.pzks.FS.actions;

import ua.kpi.fpm.pzks.FS.markers.Readable;
import ua.kpi.fpm.pzks.FS.Entity;

public class Read<T extends Entity & Readable<?>> extends FSAction<Void, T> {
    public Read(T target) {
        super(target);
    }

    public Read(String targetPath) {
        super(targetPath);
    }

    @Override
    public FSActionType getFSActionType() {
        return FSActionType.READ;
    }
}
