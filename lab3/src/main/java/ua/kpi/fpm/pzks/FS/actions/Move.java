package ua.kpi.fpm.pzks.FS.actions;

import ua.kpi.fpm.pzks.FS.Directory;
import ua.kpi.fpm.pzks.FS.Entity;
import ua.kpi.fpm.pzks.FS.exceptions.ArgumentNotSetException;
import ua.kpi.fpm.pzks.FS.exceptions.ArgumentOverrideException;
import ua.kpi.fpm.pzks.FS.exceptions.SimilarArgumentSetException;

import java.util.Optional;

public class Move extends FSAction<Directory, Entity> {
    private Directory destination;
    private String destinationPath;

    public Move(Entity target) {
        super(target);
    }

    public Move(String targetPath) {
        super(targetPath);
    }

    public void setDestinationArgument(Directory destination)
            throws ArgumentOverrideException, SimilarArgumentSetException {
        if (this.destination != null) {
            throw new ArgumentOverrideException();
        }
        if (destinationPath != null) {
            throw new SimilarArgumentSetException();
        }
        this.destination = destination;
    }

    public Optional<Directory> getDestinationArgument() throws ArgumentNotSetException {
        if (destination == null && destinationPath == null) {
            throw new ArgumentNotSetException();
        }
        return Optional.ofNullable(destination);
    }

    public void setDestinationPathArgument(String destinationPath)
            throws ArgumentOverrideException, SimilarArgumentSetException {
        if (this.destinationPath != null) {
            throw new ArgumentOverrideException();
        }
        if (destination != null) {
            throw new SimilarArgumentSetException();
        }
        this.destinationPath = destinationPath;
    }

    public Optional<String> getDestinationPathArgument() throws ArgumentNotSetException {
        if (destination == null && destinationPath == null) {
            throw new ArgumentNotSetException();
        }
        return Optional.ofNullable(destinationPath);
    }

    @Override
    public FSActionType getFSActionType() {
        return FSActionType.MOVE;
    }
}
