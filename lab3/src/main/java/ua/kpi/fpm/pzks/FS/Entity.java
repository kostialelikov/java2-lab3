package ua.kpi.fpm.pzks.FS;

import ua.kpi.fpm.pzks.FS.exceptions.InvalidPathException;
import ua.kpi.fpm.pzks.FS.exceptions.PathNotFoundException;

public abstract class Entity {
    public enum EntityType {
        DIRECTORY,
        BINARY_FILE,
        LOG_TEXT_FILE,
        BUFFER_FILE,
        NAMED_PIPE
    }

    private Directory parent;
    private String name;

    protected Entity(String name) {
        if (name == null || name.length() == 0 || name.contains("/")) {
            throw new IllegalArgumentException("Name could not be null or zero-length and contain '/'");
        }
        this.name = name;
    }

    protected Entity(String name, Directory parent) {
        this(name);
        if (parent == null) {
            throw new IllegalArgumentException("Parent directory could not be null");
        }
        parent.atomicSynchronizedOperationWithChildren(() -> {
            if (parent.containsChild(name)) {
                throw new IllegalArgumentException("Element with same name already exists");
            }
            this.parent = parent;
            parent.addChild(this);
        });
    }

    protected Directory getParent() {
        return parent;
    }

    protected void setName(String name) {
        if (name == null || name.length() == 0) {
            throw new IllegalArgumentException("Name could not be null or zero-length");
        }
        var oldName = this.name;
        this.name = name;
        if (parent != null) {
            parent.atomicSynchronizedOperationWithChildren(() -> {
                parent.updateChild(this, oldName);
            });
        }
    }

    protected Directory getRoot() {
        if (parent == null) {
            return (Directory) this;
        }
        Directory current = parent;
        while (current.getParent() != null) {
            current = current.getParent();
        }
        return current;
    }

    protected Entity retrieveEntityFromPath(String path) {
        String[] subFolders = path.split("/");
        Entity startPoint;
        int index = 0;
        if (path.startsWith("/")) {
            index++;
            startPoint = getRoot();
        } else if (path.startsWith("..")) {
            index++;
            startPoint = parent;
        } else if (path.startsWith(".")) {
            index++;
            startPoint = this;
        } else {
            startPoint = this;
        }
        return retrieveEntityFromPathHelper(startPoint, subFolders, index);
    }

    private Entity retrieveEntityFromPathHelper(Entity current, String[] subFolders, int index) {
        if (index < subFolders.length && !current.isDirectory()) {
            throw new InvalidPathException();
        }
        if (index >= subFolders.length) {
            return current;
        }
        var nextPath = subFolders[index++];
        var nextEntity = ((Directory)current).getChild(nextPath);
        if (nextEntity == null) {
            throw new PathNotFoundException();
        }
        return retrieveEntityFromPathHelper(nextEntity, subFolders, index);
    }

    public void delete() {
        if (parent != null) {
            parent.atomicSynchronizedOperationWithChildren(() -> {
                parent.removeChild(name);
                parent = null;
            });
        }
    }

    public void move(Directory destination) {
        if (destination == null) {
            throw new IllegalArgumentException("Destination could not be null");
        }
        destination.atomicSynchronizedOperationWithChildren(() -> {
            if (parent != null) {
                parent.atomicSynchronizedOperationWithChildren(() -> parent.removeChild(name));
            }
            parent = destination;
            parent.addChild(this);
        });
    }

    public void move(String destination) {
        if (destination == null) {
            throw new IllegalArgumentException("Destination could not be null");
        }
        var destinationEntity = parent.atomicSynchronizedOperationWithTree(() -> {
            var entity = retrieveEntityFromPath(destination);
            if (!entity.isDirectory()) {
                throw new IllegalArgumentException("Destination is not a directory");
            }
            return (Directory) entity;
        });
        move(destinationEntity);
    }

    public synchronized String getFullPath() {
        var sb = new StringBuilder(getName());
        if (parent == null) {
            return "/";
        }
        Directory current = parent;
        while (current.getParent() != null) {
            sb.insert(0, "/");
            sb.insert(0, current.getName());
            current = current.getParent();
        }
        sb.insert(0, "/");
        return sb.toString();
    }

    public String getName() {
        return name;
    }

    public abstract EntityType getType();

    public abstract int getSize();

    public boolean isDirectory() {
        return false;
    }
}
