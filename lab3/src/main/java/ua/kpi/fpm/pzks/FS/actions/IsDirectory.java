package ua.kpi.fpm.pzks.FS.actions;

import ua.kpi.fpm.pzks.FS.Entity;

public class IsDirectory extends FSAction<Void, Entity> {
    public IsDirectory(Entity target) {
        super(target);
    }

    public IsDirectory(String targetPath) {
        super(targetPath);
    }

    @Override
    public FSActionType getFSActionType() {
        return FSActionType.IS_DIRECTORY;
    }
}
