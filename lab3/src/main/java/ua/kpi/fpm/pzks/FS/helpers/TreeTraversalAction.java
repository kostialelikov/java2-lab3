package ua.kpi.fpm.pzks.FS.helpers;

import ua.kpi.fpm.pzks.FS.Directory;
import ua.kpi.fpm.pzks.FS.Entity;

import java.util.concurrent.RecursiveTask;
import java.util.function.BinaryOperator;
import java.util.function.Function;

public class TreeTraversalAction<R> extends RecursiveTask<R> {
    private final Directory dir;
    private final Function<Entity, R> mapEntity;
    private final BinaryOperator<R> addEntityToDir;

    public TreeTraversalAction(Directory dir, Function<Entity, R> mapEntity, BinaryOperator<R> addEntityToDir) {
        this.dir = dir;
        this.mapEntity = mapEntity;
        this.addEntityToDir = addEntityToDir;
    }

    @Override
    protected R compute() {
        var res = mapEntity.apply(dir);
        for (var entity : dir.list()) {
            if (entity.isDirectory()) {
                res = addEntityToDir.apply(res,
                        new TreeTraversalAction<>((Directory)entity, mapEntity, addEntityToDir).invoke());
            } else {
                res = addEntityToDir.apply(res, mapEntity.apply(entity));
            }
        }
        return res;
    }
}
