package ua.kpi.fpm.pzks.FS;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.ReentrantLock;

public class BufferFile<T> extends Entity {
    private final static int MAX_BUF_FILE_SIZE = 10;
    private final Queue<T> queue;
    private final ReentrantLock mutex;

    private BufferFile(String name, Directory parent) {
        super(name, parent);
        queue = new LinkedList<>();
        mutex = new ReentrantLock();
    }

    public static <T> BufferFile<T> create(String name, Directory parent) {
        return new BufferFile<>(name, parent);
    }

    public void push(T element) {
        if (element == null) {
            throw new IllegalArgumentException("Element could not be null");
        }
        mutex.lock();
        try {
            if (queue.size() >= MAX_BUF_FILE_SIZE) {
                throw new IllegalCallerException("Too many elements in buffer");
            }
            queue.add(element);
        } finally {
            mutex.unlock();
        }
    }

    public T consume() {
        mutex.lock();
        try {
            return queue.remove();
        } finally {
            mutex.unlock();
        }
    }

    @Override
    public EntityType getType() {
        return EntityType.BUFFER_FILE;
    }

    @Override
    public int getSize() {
        mutex.lock();
        try {
            return queue.size();
        } finally {
            mutex.unlock();
        }
    }
}
